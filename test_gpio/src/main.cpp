//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QCoreApplication>

#include "GPIO.h"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	/*
	// File based GPIOPin access
	GPIOPin::shared_ptr pGPIOPin(new GPIOPin(25, false));

	// now waggle it as fast as we can
	// pathethic 2.4 Khz square wave with horrendous jitter
	while (true) {
		pGPIOPin->digitalWrite(0);
		pGPIOPin->digitalWrite(1);
	}
	*/


	/*
	// Memory mapped GPIO access
	volatile unsigned* gpio( GPIOMem::setup_io() );

	// the following macros require a *gpio pointer in the current scope
	uint8_t gpioNum = 25;

	INP_GPIO(gpioNum);
	OUT_GPIO(gpioNum);

	uint32_t gp25 = 1<<gpioNum;

	// OMG a whopping 21.3MHz
	while (true) {
		GPIO_SET = gp25;
		GPIO_CLR = gp25;
	}
	*/

	// C++-ized mem mapped GPIO
	uint8_t gpioNum = 25;

	GPIO::shared_ptr pGPIO(new GPIO());

	pGPIO->setOutput(gpioNum);

	// 21.3 Mhz :)
	while (true) {
		pGPIO->digitalWrite(gpioNum, true);
		pGPIO->digitalWrite(gpioNum, false);
	}

	return 0;
}
