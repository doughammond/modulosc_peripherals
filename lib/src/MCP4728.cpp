//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "MCP4728.h"

MCP4728::MCP4728(GPIOPin::shared_ptr pGPIO, I2C::shared_ptr pI2C, QObject *parent) :
	QObject(parent),

	m_pGPIO(pGPIO),
	m_pI2C(pI2C)
{
	// Reset all power down bits
	m_PDBits.resize(4, 0);

	// Reset all VRef bits
	m_VrefBits.resize(4, 0);

	// Reset all Gx bits
	m_GxBits.resize(4, 0);
}

void MCP4728::reset() {
	m_pI2C->send(0x00);
	m_pI2C->send(DAC::GC_RESET);
}

void MCP4728::wakeUp() {
	m_pI2C->send(0x00);
	m_pI2C->send(DAC::GC_WAKEUP);
}

void MCP4728::softwareUpdate() {
	m_pI2C->send(0x00);
	m_pI2C->send(DAC::GC_SOFTUP);
}

uint8_t MCP4728::readAddressBits() {
	m_pGPIO->digitalWrite(1);
	m_pI2C->send(0x00);
	m_pI2C->send(DAC::GC_READADDR);
	m_pGPIO->digitalWrite(0);
	m_pI2C->send(DAC::DEVICE_CODE | DAC::MODE_READ);
	uint8_t out = 0;
	m_pI2C->receive(&out, 1);
	// out is EA2 EA1 EA0 1 RA2 RA1 RA0 0
	// where EAx is EEPROM value and RAx is input register value
	// we just return the EEPROM value shifted down
	return out >> 5;
}

void MCP4728::fastWrite(const uint16_t dataA, const uint16_t dataB, const uint16_t dataC, const uint16_t dataD) {
	// according to Figure 5-7

	const uint8_t _cmdFMW = DAC::CMD_FASTMODEW << 6;

	uint8_v TxBuf;
	TxBuf +=

	// 2nd Byte; C2, C1, PD1, PD0, 4MSB of dataA
	(_cmdFMW /*| (m_PDBits[0] << 4) */| (dataA >> 8)),
	// 3rd Byte; 8LSB of dataA
	(dataA & 0xFF),

	// 4th Byte; C2, C1, PD1, PD0, 4MSB of dataB
	(_cmdFMW /*| (m_PDBits[1] << 4) */| (dataB >> 8)),
	// 5th Byte; 4LSB of dataB
	(dataB & 0xFF),

	// 6th Byte; C2, C1, PD1, PD0, 4MSB of dataC
	(_cmdFMW /*| (m_PDBits[2] << 4) */| (dataC >> 8)),
	// 7th Byte; 4LSB of dataC
	(dataC & 0xFF),

	// 8th Byte; C2, C1, PD1, PD0, 4MSB of dataD
	(_cmdFMW /*| (m_PDBits[3] << 4) */| (dataD >> 8)),
	// 9th Byte; 4LSB of dataD
	(dataD & 0xFF);

	m_pI2C->send(&TxBuf[0], TxBuf.size());
}

void MCP4728::multiWrite(const DAC::addressedData_v data) {
	// According to Figure 5-8

	uint8_v TxBuf;

	const uint8_t _cmdMR = DAC::CMD_MULTIWRITE_INPUT << 3;

	for (DAC::addressedData_v::const_iterator it=data.begin(); it<data.end(); ++it) {
		const uint8_t dacAddr = it->first;
		const uint16_t dacValue = it->second;

		TxBuf +=
		// 2nd Byte; C2, C1, C0, W1, W0, DAC1, DAC0, /UDAC
		(_cmdMR | ((dacAddr & 0x03) << 1) | 0x00),
		// 3rd Byte; Vref PD1, PD0, Gx, 4MSB dacValue
		(/*(m_VrefBits[dacAddr] << 7) | (m_PDBits[dacAddr] << 5) | (m_GxBits[dacAddr] << 4) |*/ (dacValue >> 8)),
		// 4th Byte; 8LSB of dacValue
		(dacValue & 0xFF);
	}

	m_pI2C->send(&TxBuf[0], TxBuf.size());
}

void MCP4728::sequentialWrite(const uint8_t startAddr, const uint16_v data) {
	// According to Figure 5-9

	// C2, C1, C0, W1, W0, DAC1, DAC0, /UDAC
	uint8_t reg = ((DAC::CMD_SEQUNWRITE_INPUT << 3) | ((startAddr & 0x03) << 1) | 0x00);

	uint8_v TxBuf;
	for (uint8_t dacAddr=startAddr; dacAddr<4; ++dacAddr) {
		const uint16_t dacValue = data[dacAddr-startAddr];

		TxBuf +=
		// 3rd Byte; Vref PD1, PD0, Gx, 4MSB dacValue
		((m_VrefBits[dacAddr] << 7) | (m_PDBits[dacAddr] << 5) | (m_GxBits[dacAddr] << 4) | (dacValue >> 8)),
		// 4th Byte; 8LSB of dacValue
		(dacValue & 0xFF);
	}
	m_pI2C->send(reg, &TxBuf[0], TxBuf.size());
}

void MCP4728::singleWrite(const uint8_t dacAddr, const uint16_t dacValue) {
	// According to Figure 5-10

	// C2, C1, C0, W1, W0, DAC1, DAC0, /UDAC
	uint8_t reg = ((DAC::CMD_SINGLWRITE_EEPROM << 3) | ((dacAddr & 0x03) << 1) | 0x00);

	uint8_v TxBuf;
	TxBuf +=
	// 3rd Byte; Vref PD1, PD0, Gx, 4MSB dacValue
	(/*(m_VrefBits[dacAddr] << 7) | (m_PDBits[dacAddr] << 5) | (m_GxBits[dacAddr] << 4) |*/ (dacValue >> 8)),
	// 4th Byte; 8LSB of dacValue
	(dacValue & 0xFF);

	m_pI2C->send(reg, &TxBuf[0], TxBuf.size());
}
