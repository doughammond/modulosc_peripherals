//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "LCDDriver.h"

LCDDriver::LCDDriver(GPIOPin::shared_ptr pGPIO, I2C::shared_ptr pI2C, QObject *parent) :
	QObject(parent),

	m_pGPIO(pGPIO),
	m_pI2C(pI2C),

	// I2C is relatively slow.  MCP output port states are cached
	// so we don't need to constantly poll-and-change bit states.
	// A = 0xC0 - set R, G LED bits high (LEDs off); inverse of 0x3F
	// B = 0xEF - set all except LCD D7 high (blue LED off); is inverse of m_ddrb
	m_portA(0xC0), m_portB(0xEF), m_ddrb(0x10),

	// set default LCD bahaviours
	m_displayShift(LCDPlate::CURSORMOVE | LCDPlate::MOVERIGHT),
	m_displayMode(LCDPlate::ENTRYLEFT | LCDPlate::ENTRYSHIFTDECREMENT),
	m_displayControl(LCDPlate::DISPLAYON | LCDPlate::CURSOROFF | LCDPlate::BLINKOFF),

	m_currLine(0), m_numLines(0)
{
	// Set MCP23017 IOCON register to Bank 0 with sequential operation.
	// If chip is already set for Bank 0, this will just write to OLATB,
	// which won't seriously bother anything on the plate right now
	// (blue backlight LED will come on, but that's done in the next
	// step anyway).
	m_pI2C->send(MCP23017::IOCON_BANK1, 0x00);
	_logI2CFail();

	// Brute force reload ALL registers to known state.  This also
	// sets up all the input pins, pull-ups, etc. for the Pi Plate.
	uint8_t _registerInit[] = {
		0x3F,    // IODIRA    R+G LEDs=outputs, buttons=inputs
		m_ddrb,  // IODIRB    LCD D7=input, Blue LED=output
		0x3F,    // IPOLA     Invert polarity on button inputs
		0x00,    // IPOLB
		0x3F,    // GPINTENA  interrupt-on-change
		0x00,    // GPINTENB
		0x00,    // DEFVALA
		0x00,    // DEFVALB
		0x00,    // INTCONA
		0x00,    // INTCONB
		0x02,    // IOCON     BANK, MIRROR, SEQOP, DISSLW, HAEN, ODR, INTPOL, -
		0x02,    // IOCON
		0x3F,    // GPPUA     Enable pull-ups on buttons
		0x00,    // GPPUB
		0x00,    // INTFA
		0x00,    // INTFB
		0x00,    // INTCAPA
		0x00,    // INTCAPB
		m_portA, // GPIOA    Initial states for GPIO ports
		m_portB, // GPIOB
		m_portA, // OLATA
		m_portB  // OLATB
	};
	m_pI2C->send(0x00, _registerInit, 22);
	_logI2CFail();

	// Switch to Bank 1 and disable sequential operation.
	// From this point forward, the register addresses do NOT match
	// the list immediately above.  Instead, use the constants defined
	// at the start of the class.  Also, the address register will no
	// longer increment automatically after this -- multi-byte
	// operations must be broken down into single-byte calls.
	m_pI2C->send(MCP23017::IOCON_BANK0, 0xA2); // Originally 0xA0
	_logI2CFail();

	write(0x33); // Init
	write(0x32); // Init
	write(0x28); // 2 line 5x8 matrix
	write(LCDPlate::CLEARDISPLAY);
	write(LCDPlate::CURSORSHIFT    | m_displayShift);
	write(LCDPlate::ENTRYMODESET   | m_displayMode);
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
	write(LCDPlate::RETURNHOME);


	connect(this, SIGNAL(buttonDown(uint8_t)), this, SLOT(logButtonDown(uint8_t)));
	connect(this, SIGNAL(buttonUp(uint8_t)), this, SLOT(logButtonUp(uint8_t)));
	connect(this, SIGNAL(buttonDown(uint8_t)), this, SLOT(demuxButtonPress(uint8_t)));

	connect(&m_tmr, SIGNAL(timeout()), this, SLOT(pollInterrupt()));
	m_tmr.start(5);
}

// Puts the MCP23017 back in Bank 0 + sequential write mode so
// that other code using the 'classic' library can still work.
// Any code using this newer version of the library should
// consider adding an atexit() handler that calls this
void LCDDriver::stop() {
	m_portA = 0xC0;		// Turn off LEDs on the way out
	m_portB = 0x01;

	m_pI2C->send(MCP23017::IOCON_BANK1, 0x00);
	_logI2CFail();

	uint8_t _registerInit[] = {
		0x3F,    // IODIRA    R+G LEDs=outputs, buttons=inputs
		m_ddrb,  // IODIRB    LCD D7=input, Blue LED=output
		0x00,    // IPOLA     Invert polarity on button inputs
		0x00,    // IPOLB
		0x00,    // GPINTENA  Disable interrupt-on-change
		0x00,    // GPINTENB
		0x00,    // DEFVALA
		0x00,    // DEFVALB
		0x00,    // INTCONA
		0x00,    // INTCONB
		0x00,    // IOCON
		0x00,    // IOCON
		0x3F,    // GPPUA
		0x00,    // GPPUB
		0x00,    // INTFA
		0x00,    // INTFB
		0x00,    // INTCAPA
		0x00,    // INTCAPB
		m_portA, // GPIOA
		m_portB, // GPIOB
		m_portA, // OLATA
		m_portB  // OLATB
	};
	m_pI2C->send(0x00, _registerInit, 22);
	_logI2CFail();
}

uint8_t LCDDriver::_writePoll(const bool char_mode) {
	// If pin D7 is in input state, poll LCD busy flag until clear.
	if ((m_ddrb & 0x10) != 0) {
		uint8_t lo = (m_portB & 0x01) | 0x40;
		uint8_t hi = lo | 0x20;
		m_pI2C->send(MCP23017::GPIOB, lo);
		_logI2CFail();
		while (true) {
			// strobe high (enable)
			m_pI2C->send(MCP23017::GPIOB, hi);
			_logI2CFail();
			// First nybble contains busy state
			uint8_t bits; // = 0x00;
			m_pI2C->receive(&bits, 1);
			_logI2CFail();
			// Strobe low, high, low.  Second nybble (A3) is ignored
			uint8_t _bobs[] = { lo, hi, lo };
			m_pI2C->send(MCP23017::GPIOB, _bobs, 3);
			_logI2CFail();
			if ((bits & 0x02) == 0) {
				break;
			}
		}
		m_portB = lo;

		// Polling complete, change D7 pin to output
		m_ddrb &= 0xEF;
		m_pI2C->send(MCP23017::IODIRB, m_ddrb);
		_logI2CFail();
	}
	uint8_t bitmask = m_portB & 0x01;	// Mask out PORTB LCD control bits
	if (char_mode) {
		bitmask |= 0x80; // Set data bit if not a command
	}

	return bitmask;
}

void LCDDriver::_logI2CFail() {
	if (m_pI2C->fail()) {
		std::cout << "I2C " << m_pI2C->getErrorMessage() << std::endl;
	}
}

/**
 * Send string data to LCD
 */
void LCDDriver::write(const std::string value, const bool char_mode)
{
	uint8_t bitmask = _writePoll(char_mode);

	size_t last = value.length() - 1;
	uint8_v data;
	for (size_t i=0; i<value.length(); ++i) {
		// Append 4 bytes to list representing PORTB over time.
		// First the high 4 data bits with strobe (enable) set
		// and unset, then same with low 4 data bits (strobe 1/0).
		uint8_v _block = createBlock(bitmask, value[i]);
		data.insert(data.end(), _block.begin(), _block.end());

		// I2C block data write is limited to 32 bytes max.
		// If limit reached, write data so far and clear.
		// Also do this on last byte if not otherwise handled.
		if (data.size() >= 32 || (i == last)) {
			m_pI2C->send(MCP23017::GPIOB, &data[0], data.size());
			_logI2CFail();
			m_portB = data[data.size() - 1];	// Save state of last byte out
			data.clear();						// Clear list for next iteration
		}
	}
}

/**
 * Send vector of data to LCD
 */
void LCDDriver::write(const uint8_v block, const bool char_mode)
{
	uint8_t bitmask = _writePoll(char_mode);

	size_t last = block.size() - 1;
	uint8_v data;
	for (size_t i=0; i<block.size(); ++i) {
		// Append 4 bytes to list representing PORTB over time.
		// First the high 4 data bits with strobe (enable) set
		// and unset, then same with low 4 data bits (strobe 1/0).
		uint8_v _block = createBlock(bitmask, block[i]);
		data.insert(data.end(), _block.begin(), _block.end());

		// I2C block data write is limited to 32 bytes max.
		// If limit reached, write data so far and clear.
		// Also do this on last byte if not otherwise handled.
		if (data.size() >= 32 || (i == last)) {
			m_pI2C->send(MCP23017::GPIOB, &data[0], data.size());
			_logI2CFail();
			m_portB = data[data.size() - 1];	// Save state of last byte out
			data.clear();						// Clear list for next iteration
		}
	}
}

/**
 * Send single byte (command) to LCD
 */
void LCDDriver::write(const uint8_t value, const bool char_mode)
{
	uint8_t bitmask = _writePoll(char_mode);

	uint8_v data = createBlock(bitmask, value);
	m_pI2C->send(MCP23017::GPIOB, &data[0], data.size());
	_logI2CFail();
	m_portB = data[data.size() - 1];	// Save state of last byte out

	// If a poll-worthy instruction was issued, reconfigure D7
	// pin as input to indicate need for polling on next call.
	if (!char_mode && (value == LCDPlate::CLEARDISPLAY || value == LCDPlate::RETURNHOME)) {
		m_ddrb |= 0x10;
		m_pI2C->send(MCP23017::IODIRB, m_ddrb);
		_logI2CFail();
	}
}

void LCDDriver::begin(const uint8_t cols, const uint8_t lines) {
	m_currLine = 0;
	m_numLines = lines;
	clear();
}

void LCDDriver::clear() {
	write(LCDPlate::CLEARDISPLAY);
}

void LCDDriver::home() {
	write(LCDPlate::RETURNHOME);
}

void LCDDriver::setCursor(const uint8_t col, uint8_t row) {
	if (row > m_numLines) {
		row = m_numLines - 1;
	}
	write(LCDPlate::SETDDRAMADDR | (col + LCDPlate::ROW_OFFSETS[row]));
}

void LCDDriver::display() {
	m_displayControl |= LCDPlate::DISPLAYON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::noDisplay() {
	m_displayControl &= ~LCDPlate::DISPLAYON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::cursor() {
	m_displayControl |= LCDPlate::CURSORON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::noCursor() {
	m_displayControl &= ~LCDPlate::CURSORON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::toggleCursor() {
	m_displayControl ^= LCDPlate::CURSORON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::blink() {
	m_displayControl |= LCDPlate::BLINKON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::noBlink() {
	m_displayControl &= ~LCDPlate::BLINKON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::toggleBlink() {
	m_displayControl ^= LCDPlate::BLINKON;
	write(LCDPlate::DISPLAYCONTROL | m_displayControl);
}

void LCDDriver::scrollDisplayLeft() {
	m_displayShift = LCDPlate::DISPLAYMOVE | LCDPlate::MOVELEFT;
	write(LCDPlate::CURSORSHIFT | m_displayShift);
}

void LCDDriver::scrollDisplayRight() {
	m_displayShift = LCDPlate::DISPLAYMOVE | LCDPlate::MOVERIGHT;
	write(LCDPlate::CURSORSHIFT | m_displayShift);
}

void LCDDriver::leftToRight() {
	m_displayMode |= LCDPlate::ENTRYLEFT;
	write(LCDPlate::ENTRYMODESET | m_displayMode);
}

void LCDDriver::rightToLeft() {
	m_displayMode &= ~LCDPlate::ENTRYLEFT;
	write(LCDPlate::ENTRYMODESET | m_displayMode);
}

void LCDDriver::autoscroll() {
	m_displayMode |= LCDPlate::ENTRYSHIFTINCREMENT;
	write(LCDPlate::ENTRYMODESET | m_displayMode);
}

void LCDDriver::noAutoscroll() {
	m_displayMode &= ~LCDPlate::ENTRYSHIFTINCREMENT;
	write(LCDPlate::ENTRYMODESET | m_displayMode);
}

void LCDDriver::createChar(uint8_t location, uint8_v bitmap) {
	write(LCDPlate::SETCGRAMADDR | ((location & 7) << 3));
	write(bitmap, true);
	write(LCDPlate::SETDDRAMADDR);
}

uint8_t LCDDriver::buttonPressed(const uint8_t b) {
	uint8_t _in[] = { 0x00 };
	m_pI2C->receive(MCP23017::GPIOA, _in, 1);
	_logI2CFail();
	return (_in[0] >> b) & 1;
}

uint8_t LCDDriver::buttons() {
	uint8_t _in[] = { 0x00 };
	m_pI2C->receive(MCP23017::GPIOA, _in, 1);
	_logI2CFail();
	return _in[0] & 0x1F;
}

void LCDDriver::message(const QString text) {
	clear();
	std::cout << text.toStdString() << std::endl;

	int i = 0;
	// use stringstream to split text into lines
	std::istringstream stream(text.toStdString());
	std::string line;
	// for each line
	while (std::getline(stream, line)) {
		if (i > 0) {
			// If not the first line, then
			// set DDRAM address to 2nd line
			write(0xC0);
		}
		// write the line
		write(line, true);

		i++;
	}
}

void LCDDriver::backlight(const uint8_t color) {
	std::cout << "LCD: set backlight " << numberToString(color) << std::endl;
	uint8_t c = ~color;
	m_portA = (m_portA & 0x3F) | ((c & 0x03) << 6);
	m_portB = (m_portB & 0xFE) | ((c & 0x04) >> 2);
	// has to be done as two writes because sequential operation is off
	m_pI2C->send(MCP23017::GPIOA, m_portA);
	_logI2CFail();
	m_pI2C->send(MCP23017::GPIOB, m_portB);
	_logI2CFail();
}

void LCDDriver::pollInterrupt() {
	int btnIntr = m_pGPIO->digitalRead();

	if (btnIntr > 0) {
		readInterrupt();
	}
}

void LCDDriver::readInterrupt() {
	uint8_t _in[] = { 0x00, 0x00 };
	m_pI2C->receive(MCP23017::INTFA, _in, 1);
	_logI2CFail();

	m_pI2C->receive(MCP23017::INTCAPA, _in + 1, 1);
	_logI2CFail();

	// work out what changed
	// INTFA is the address of the button which caused the interrupt
	// INTCAPA is the bitmask of currently pressed buttons;
	// therefore logical AND of these two tells us the current state
	// of the button which caused the interrupt.

	if (_in[0] & _in[1]) {
		// button down event
		emit buttonDown(_in[0]);
	} else if (_in[0] != 0) {
		// button up event
		emit buttonUp(_in[0]);
	}
}

void LCDDriver::demuxButtonPress(const uint8_t buttonNumber) {
	switch (buttonNumber) {
		case LCDPlate::BUTTON_SELECT:
			emit btnPressSelect();
			break;
		case LCDPlate::BUTTON_UP:
			emit btnPressUp();
			break;
		case LCDPlate::BUTTON_DOWN:
			emit btnPressDown();
			break;
		case LCDPlate::BUTTON_LEFT:
			emit btnPressLeft();
			break;
		case LCDPlate::BUTTON_RIGHT:
			emit btnPressRight();
			break;
	}
}

void LCDDriver::logButtonDown(const uint8_t buttonNumber) {
	std::cout << "BUTTON DOWN " << static_cast<int>(buttonNumber) << std::endl;
}

void LCDDriver::logButtonUp(const uint8_t buttonNumber) {
	std::cout << "BUTTON UP   " << static_cast<int>(buttonNumber) << std::endl;
}

uint8_v LCDDriver::createBlock(const uint8_t bitmask, const uint8_t value) {
	uint8_t hi = bitmask | LCDPlate::FLIP[value >> 4];
	uint8_t lo = bitmask | LCDPlate::FLIP[value & 0x0F];
	uint8_v out;
	out += hi | 0x20, hi, lo | 0x20, lo;
	return out;
}
