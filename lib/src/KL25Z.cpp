#include "KL25Z.h"

void KL25Z_SPI_Reader::run() {
	if (spi_fd && !status) {
		static const uint8_t PACKET_LENGTH = 8;

		while (true) {	// TODO implement stop condition
			uint8_t rx[PACKET_LENGTH] = { 0, };

			// reserve the right to fill PACKET_SIZE bytes in the buffer
			m_pFreeBytes->acquire(PACKET_LENGTH);

			struct spi_ioc_transfer msg;

			msg.tx_buf = NULL;	// will auto-fill zeros for tx
			msg.rx_buf = (unsigned long)rx;
			msg.len = PACKET_LENGTH;
			msg.delay_usecs = 0;
			msg.speed_hz = spi_speed;
			msg.bits_per_word = spi_bitsPerWord;
			msg.cs_change = 0;

			// perform the SPI transaction
			ioctl(spi_fd, SPI_IOC_MESSAGE(1), &msg);

			for (int i=0; i<PACKET_LENGTH; ++i) {
				m_pBuffer->at(bufferIndex) = rx[i];
				bufferIndex = (bufferIndex + 1) % BufferSize;
			}

			m_pUsedBytes->release(PACKET_LENGTH);
		}
	}
}


KL25Z::KL25Z(QObject *parent) :
	QObject(parent),
	m_pReader(new KL25Z_SPI_Reader()),
	bufferIndex(0)
{
	connect(&m_dataTimer, SIGNAL(timeout()), this, SLOT(tmrGetData()));
	m_dataTimer.start(100);

	QThreadPool::globalInstance()->start(m_pReader.get());
}

void KL25Z::tmrGetData()
{
	m_pReader->getUsedBytesSem()->acquire();
	const uint8_t val = m_pReader->getBuffer()->at(bufferIndex);

	std::cout /*<< bufferIndex << "="*/ << static_cast<unsigned>(val) << " ";
	if (bufferIndex % 16 == 0) std::cout << std::endl;
	std::cout.flush();

	bufferIndex = (bufferIndex + 1) % BufferSize;
	m_pReader->getFreeBytesSem()->release();
}
