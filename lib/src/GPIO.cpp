//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "GPIO.h"

int stringToNumber(std::string str){
	std::stringstream  strin;
	int var;

	strin << str;
	strin >> var;

	return var;
}

std::string numberToString(int num){
	std::ostringstream strout;
	std::string str;

	strout << num;
	str = strout.str();

	return str;
}

GPIOPin::GPIOPin(const uint8_t pinNumber, const bool isInput, QObject *parent) :
	QObject(parent),
	m_exported(false),
	m_valid(false),
	m_pinNo(pinNumber),
	m_isInput(isInput),
	m_pinFileName("/sys/class/gpio/gpio"+numberToString(pinNumber)+"/value")
{
	std::string pin_str = numberToString(pinNumber);
	std::string dir = "/sys/class/gpio/export";
	std::ofstream file (dir.c_str());
	if (file < 0) {
		std::cout << "GPIO: cannot export pin #" << pin_str << std::endl;
		return;
	}
	file << pin_str;
	file.close();
	m_exported = true;

	dir = "/sys/class/gpio/gpio" + pin_str + "/direction";

	file.open(dir.c_str());
	if (file < 0) {
		std::cout << "GPIO: cannot open file " << dir << std::endl;
		return;
	}
	file << (isInput ? "in" : "out");
	file.close();

	m_valid = true;
}

GPIOPin::~GPIOPin() {
	if (!m_exported) return;

	std::string pin_str = numberToString(m_pinNo);
	std::string dir = "/sys/class/gpio/unexport";
	std::ofstream file (dir.c_str());
	if (file < 0) {
		return;
	}
	file << pin_str;
	file.close();
}

int GPIOPin::digitalWrite(const bool value) {
	if (!m_valid) return -1;
	if (m_isInput) return -2;

	std::string value_str = numberToString(value);

	std::ofstream file(m_pinFileName.c_str());
	if (file < 0) {
		return -1;
	}
	file << value_str;
	file.close();

	return 1;
}

int GPIOPin::digitalRead() {
#ifdef I2C_DEBUG
	return -1;
#endif
	if (!m_valid) return -1;
	if (!m_isInput) return -1;

	std::string value;

	std::ifstream file(m_pinFileName.c_str());
	if (file < 0){
		return -1;
	}
	file >> value;
	file.close();

	return stringToNumber(value);
}


GPIO::GPIO(QObject *parent) :
	QObject(parent)
{
	int  mem_fd;

	// open /dev/mem
	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
		std::cout << "can't open /dev/mem" << std::endl;
		return;
	}

	// mmap GPIO
	m_mappedGPIO = static_cast<volatile unsigned*>(mmap(
		NULL,					// Any adddress in our space will do
		GPIOMem::BLOCK_SIZE,	// Map length
		PROT_READ|PROT_WRITE,	// Enable reading & writting to mapped memory
		MAP_SHARED,				// Shared with other processes
		mem_fd,					// File to map
		GPIOMem::GPIO_BASE		// Offset to GPIO peripheral
	));

	//No need to keep mem_fd open after mmap
	close(mem_fd);

	if (m_mappedGPIO == MAP_FAILED) {
		//errno also set!
		std::cout << "mmap error" /*<< numberToString(static_cast<int>(m_mappedGPIO))*/ << std::endl;
	}
}

GPIO::~GPIO()
{
	// unmap the gpio region from /dev/mem
	munmap((void*)m_mappedGPIO, GPIOMem::BLOCK_SIZE);
}

