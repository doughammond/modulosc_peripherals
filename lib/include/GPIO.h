//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 *
 * GPIO Pin driver; heavily influenced by gnublin-api, uses /sys/class/gpio
 * access to configure, read and write pins.
 *
 */

#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdint.h>

#include <boost/shared_ptr.hpp>

#include <QObject>

int stringToNumber(std::string str);
std::string numberToString(int num);

class GPIOPin : public QObject
{
Q_OBJECT

public:
	typedef boost::shared_ptr<GPIOPin> shared_ptr;

	explicit GPIOPin(const uint8_t pinNumber, const bool isInput, QObject *parent = 0);
	~GPIOPin();

	int digitalWrite(const bool value);
	int digitalRead();

private:
	bool m_exported;
	bool m_valid;

	uint8_t m_pinNo;
	bool m_isInput;

	std::string m_pinFileName;
};


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

namespace GPIOMem
{

	//  How to access GPIO registers from C++ code on the Raspberry-Pi
	//  Doug Hammond 2014/02/04
	//  Based on
	//  How to access GPIO registers from C-code on the Raspberry-Pi
	//  15-January-2012
	//  Dom and Gert
	//  Revised: 15-Feb-2013

	// Access from ARM Running Linux

	// Peripherals base
	static const unsigned BCM2708_PERI_BASE = 0x20000000;

	// GPIO controller
	static const unsigned GPIO_BASE         = (BCM2708_PERI_BASE + 0x200000);

	static const unsigned PAGE_SIZE  = 4*1024;
	static const unsigned BLOCK_SIZE = 4*1024;

} // namespace GPIOMem


class GPIO : public QObject
{
Q_OBJECT

public:
	typedef boost::shared_ptr<GPIO> shared_ptr;

	explicit GPIO(QObject *parent = 0);
	~GPIO();

	inline void setInput(const uint8_t gpioNumber) {
		*(m_mappedGPIO+((gpioNumber)/10)) &= ~(7<<(((gpioNumber)%10)*3));
	}

	// must call setInput inside setOutput
	inline void setOutput(const uint8_t gpioNumber) {
		setInput(gpioNumber);
		*(m_mappedGPIO+((gpioNumber)/10)) |=  (1<<(((gpioNumber)%10)*3));
	}

	// must call setInput inside setOutputAlt
	inline void setOutputAlt(const uint8_t gpioNumber, const uint8_t alt) {
		setInput(gpioNumber);
		*(m_mappedGPIO+(((gpioNumber)/10))) |= (((alt)<=3?(alt)+4:(alt)==4?3:2)<<(((gpioNumber)%10)*3));
	}

	inline void digitalWrite(const uint8_t gpioNumber, const bool value) {
		const uint32_t gpbit = 1<<gpioNumber;

		if (value) {
			// sets bits which are 1 ignores bits which are 0
			*(m_mappedGPIO+7) = gpbit;
		} else {
			// clears bits which are 1 ignores bits which are 0
			*(m_mappedGPIO+10) = gpbit;
		}
	}

	inline bool digitalRead(const uint8_t gpioNumber) {
		// not sure how... ?
		return false;
	}

private:
	volatile unsigned* m_mappedGPIO;

};
