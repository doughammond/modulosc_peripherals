//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/shared_ptr.hpp>

#include <QObject>
#include <QTimer>

#include "GPIO.h"
#include "I2C.h"

namespace MCP23017 {
	static const uint8_t IOCON_BANK0 = 0x0A;
	static const uint8_t IOCON_BANK1 = 0x15;

	static const uint8_t GPIOA  = 0x09;
	static const uint8_t GPIOB  = 0x19;
	static const uint8_t IODIRB = 0x10;

	// GPIO port interrupt address registers
	static const uint8_t INTFA = 0x07;
	static const uint8_t INTFB = 0x17;

	// GPIO port interrupt state registers
	static const uint8_t INTCAPA = 0x08;
	static const uint8_t INTCAPB = 0x18;
} // namespace MCP23017

namespace LCDPlate {
	// humanized button numbering
	static const uint8_t SELECT = 0;
	static const uint8_t RIGHT  = 1;
	static const uint8_t DOWN   = 2;
	static const uint8_t UP     = 3;
	static const uint8_t LEFT   = 4;

	// GPIO button numbering
	static const uint8_t BUTTON_SELECT = 0x01;
	static const uint8_t BUTTON_RIGHT  = 0x02;
	static const uint8_t BUTTON_DOWN   = 0x04;
	static const uint8_t BUTTON_UP     = 0x08;
	static const uint8_t BUTTON_LEFT   = 0x10;

	// Backlight colours
	static const uint8_t ALL_OFF = 0x00;
	static const uint8_t RED     = 0x01;
	static const uint8_t GREEN   = 0x02;
	static const uint8_t BLUE    = 0x04;
	static const uint8_t YELLOW  = 0x03;
	static const uint8_t TEAL    = 0x06;
	static const uint8_t VIOLET  = 0x05;
	static const uint8_t WHITE   = 0x07;
	static const uint8_t ALL_ON  = 0x07;

	// LCD Commands
	static const uint8_t CLEARDISPLAY   = 0x01;
	static const uint8_t RETURNHOME     = 0x02;
	static const uint8_t ENTRYMODESET   = 0x04;
	static const uint8_t DISPLAYCONTROL = 0x08;
	static const uint8_t CURSORSHIFT    = 0x10;
	static const uint8_t FUNCTIONSET    = 0x20;
	static const uint8_t SETCGRAMADDR   = 0x40;
	static const uint8_t SETDDRAMADDR   = 0x80;

	// Flags for display on/off control
	static const uint8_t DISPLAYON  = 0x04;
	static const uint8_t DISPLAYOFF = 0x00;
	static const uint8_t CURSORON   = 0x02;
	static const uint8_t CURSOROFF  = 0x00;
	static const uint8_t BLINKON    = 0x01;
	static const uint8_t BLINKOFF   = 0x00;

	// Flags for display entry mode
	static const uint8_t ENTRYRIGHT          = 0x00;
	static const uint8_t ENTRYLEFT           = 0x02;
	static const uint8_t ENTRYSHIFTINCREMENT = 0x01;
	static const uint8_t ENTRYSHIFTDECREMENT = 0x00;

	// Flags for display/cursor shift
	static const uint8_t DISPLAYMOVE = 0x08;
	static const uint8_t CURSORMOVE  = 0x00;
	static const uint8_t MOVERIGHT   = 0x04;
	static const uint8_t MOVELEFT    = 0x00;

	// The LCD data pins (D4-D7) connect to MCP pins 12-9 (PORTB4-1), in
	// that order.  Because this sequence is 'reversed,' a direct shift
	// won't work.  This table remaps 4-bit data values to MCP PORTB
	// outputs, incorporating both the reverse and shift.
	static const uint8_t FLIP[16] = {
		0x00, 0x10, 0x08, 0x18, 0x04, 0x14, 0x0c, 0x1c, 0x02, 0x12, 0x0a, 0x1a, 0x06, 0x16, 0x0e, 0x1e
	};

	static const uint8_t ROW_OFFSETS[4] = {
		0x00, 0x40, 0x14, 0x54
	};
} // namespace LCDPlate

class LCDDriver : public QObject
{
Q_OBJECT

public:

	typedef boost::shared_ptr<LCDDriver> shared_ptr;

	explicit LCDDriver(GPIOPin::shared_ptr pGPIO, I2C::shared_ptr pI2C, QObject* parent=0);

	void stop();
	void write(const std::string value, const bool char_mode=false);
	void write(const uint8_v block, const bool char_mode=false);
	void write(const uint8_t value, const bool char_mode=false);
	void begin(const uint8_t cols, const uint8_t lines);
	void clear();
	void home();
	void setCursor(const uint8_t col, uint8_t row);
	void display();
	void noDisplay();
	void cursor();
	void noCursor();
	void toggleCursor();
	void blink();
	void noBlink();
	void toggleBlink();
	void scrollDisplayLeft();
	void scrollDisplayRight();
	void leftToRight();
	void rightToLeft();
	void autoscroll();
	void noAutoscroll();
	void createChar(uint8_t location, uint8_v bitmap);

	// Check to see if button numbered b is pressed
	uint8_t buttonPressed(const uint8_t b);

	// Read immediately the current state of the buttons
	uint8_t buttons();

public slots:

	/**
	 * @brief message; set the LCD message. The display is cleared first.
	 * @param text message to display on screen. May be up to 2x lines of 16 chars.
	 */
	void message(const QString text);

	// Set the LCD backlight colour; see LCDPlate "Backlight Colours"
	void backlight(const uint8_t color);

signals:

	// Signal emitted when any button is pressed; signal carries the button number
	void buttonDown(const uint8_t buttonNumber);

	// Signal emitted when any button is released; signal carries the button number
	void buttonUp(const uint8_t buttonNumber);

	// Signal emitted when the SELECT button is pressed
	void btnPressSelect();

	// Signal emitted when the UP button is pressed
	void btnPressUp();

	// Signal emitted when the DOWN button is pressed
	void btnPressDown();

	// Signal emitted when the LEFT button is pressed
	void btnPressLeft();

	// Signal emitted when the RIGHT button is pressed
	void btnPressRight();

protected:

private slots:

	/**
	 * @brief pollInterrupt; poll the GPIO pin to see if it is high
	 * meaning that there is button information to be read from the
	 * MCP23017
	 */
	void pollInterrupt();

	/**
	 * @brief readInterrupt; read the button interrupt registers from
	 * the MCP23017 and emit appropriate button down/up signals
	 */
	void readInterrupt();

	/**
	 * @brief demuxButtonPress; emit button specific signals by listening to buttonDown(buttonNumber)
	 * @param buttonNumber, see LCDPlate::BUTTON_*
	 */
	void demuxButtonPress(const uint8_t buttonNumber);

	// send the button down number to stdout
	void logButtonDown(const uint8_t buttonNumber);

	// send the button up number to stdout
	void logButtonUp(const uint8_t buttonNumber);

private:

	// helper for sending data to the LCD module
	uint8_v createBlock(const uint8_t bitmask, const uint8_t value);

	// poll the LCD to see if it is in read or write mode
	uint8_t _writePoll(const bool char_mode);

	// output last I2C failure to stdout
	inline void _logI2CFail();

private:
	// GPIO interface
	GPIOPin::shared_ptr m_pGPIO;

	// I2C interface
	I2C::shared_ptr m_pI2C;

	// port state
	uint8_t m_portA, m_portB, m_ddrb;

	// display state
	uint8_t m_displayShift, m_displayMode, m_displayControl;

	// column / line handling
	uint8_t m_currLine, m_numLines;

	// button interrupt timer
	QTimer m_tmr;
};
