//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <fcntl.h>
#include <fstream>
#include <linux/i2c-dev.h>
#include <sstream>
#include <sys/ioctl.h>

#include <stdint.h>
#include <vector>

#include <boost/shared_ptr.hpp>

// Support vector += x, y, z;
#include <boost/assign/std/vector.hpp>
using namespace boost::assign;

#include "intarrays.h"

namespace gnublin {
// -- Liberated from the gnublin-api project LGPL3 ---------------------------
// -- https://github.com/embeddedprojects/gnublin-api ------------------------
int stringToNumber(std::string str);
std::string numberToString(int num);
int hexstringToNumber(std::string str);

class gnublin_i2c {
protected:
	bool error_flag;
	int slave_address;
	std::string devicefile;
	std::string ErrorMessage;
		int fd;
		virtual int errorMsg(std::string message);
		virtual int open_fd();
		virtual void close_fd();
	virtual void init(std::string DeviceFile, int Address);
public:
	gnublin_i2c();
	gnublin_i2c(int Address);
	gnublin_i2c(std::string DeviceFile, int Address);
	~gnublin_i2c();
	virtual bool fail();
	virtual int setAddress(int Address);
	virtual int getAddress();
	virtual const char *getErrorMessage();
	virtual int setDevicefile(std::string filename);
	virtual int receive(unsigned char *RxBuf, int length);
	virtual int receive(unsigned char RegisterAddress, unsigned char *RxBuf, int length);
	virtual int send(unsigned char *TxBuf, int length);
	virtual int send(unsigned char RegisterAddress, unsigned char *TxBuf, int length);
	virtual int send(unsigned char value);
	virtual int send(unsigned char RegisterAddress, unsigned char value);
};
// ---------------------------------------------------------------------------
} // namespace gnublin

/**
 * I2C driver
 */
class I2C : public gnublin::gnublin_i2c {
public:
	typedef boost::shared_ptr<I2C> shared_ptr;

	explicit I2C(int Address) :
		gnublin::gnublin_i2c(Address) {
	}

};
