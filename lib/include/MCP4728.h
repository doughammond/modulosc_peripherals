//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <QObject>

#include <boost/shared_ptr.hpp>

#include "GPIO.h"
#include "I2C.h"

namespace DAC {

// Device DAC registers
static const uint8_t DAC_A = 0x00;
static const uint8_t DAC_B = 0x01;
static const uint8_t DAC_C = 0x02;
static const uint8_t DAC_D = 0x03;

// pair holding (Address, Data)
typedef std::pair<uint8_t, uint16_t> addressedData_t;
// vector of above pairs for sending multiple data to the DACs
typedef std::vector<addressedData_t> addressedData_v;

// Device command setups

static const uint8_t DEVICE_CODE = 0xC0;	// first 4 bits of address byte, always 1100xxxx

static const uint8_t MODE_READ  = 0x01;		// final bit of address byte, signalling device read
static const uint8_t MODE_WRITE = 0x00;		// final bit of address byte, signalling device write

static const uint8_t DEVICE_HS  = 0x08;		// singal device to enter high speed (3.4Mbit/s) mode

/// --- "General call" constants --- Data sheet 5.4 ---

// Similar to POR; EEPROM is loaded into DAC output immediately
static const uint8_t GC_RESET  = 0x06;

// Reset the power-down bits
static const uint8_t GC_WAKEUP = 0x09;

// Software update - which actually means to update all 4 DAC outputs at once
static const uint8_t GC_SOFTUP = 0x08;

// Read the device's I2C address bits; note must be used with the /LDAC pin
static const uint8_t GC_READADDR = 0x0C;

/// --- EEPROM Read/Write constants --- Data sheet 5.5 ---

//														C2 C1 C0 W1 W0
// Fast mode write
// XXX confirm ?!
static const uint8_t CMD_FASTMODEW = 0x00;			//	0  0  X  -  -

// Write DAC Input Register and EEPROM
static const uint8_t CMD_MULTIWRITE_INPUT  = 0x08;	//	0  1  0  0  0
static const uint8_t CMD_SEQUNWRITE_INPUT  = 0x0A;	//	0  1  0  1  0
static const uint8_t CMD_SINGLWRITE_EEPROM = 0x0B;	//	0  1  0  1  1

// Write I2C Address bits
static const uint8_t CMD_WRITE_ADDR = 0x0C;			//	0  1  1  -  -

// Write Vref, Gain and Power-down select bits
static const uint8_t CMD_WRITE_REF  = 0x10;			//	1  0  0  -  -
static const uint8_t CMD_WRITE_GAIN = 0x18;			//	1  1  0  -  -
static const uint8_t CMD_WRITE_PWRD = 0x14;			//	1  0  1  -  -

}	// namespace DAC

/**
 * @brief The MCP4728 class, driver for MCP4728 Quad 12-bit I2C DAC
 * http://ww1.microchip.com/downloads/en/DeviceDoc/22187a.pdf
 */
class MCP4728 : public QObject
{
Q_OBJECT

public:

	typedef boost::shared_ptr<MCP4728> shared_ptr;

	explicit MCP4728(GPIOPin::shared_ptr pGPIO, I2C::shared_ptr pI2C, QObject *parent = 0);

	// Write a new I2C device address
	void writeAddressBits(uint8_t newAddr);

	/// --- "General calls" --- Data sheet 5.4 ---

	// Data sheet 5.4.1
	void reset();

	// Data sheet 5.4.2
	void wakeUp();

	// Data sheet 5.4.3
	void softwareUpdate();

	// Data sheet 5.4.4
	// Read the device's I2C address; requires /LDAC cooperation
	uint8_t readAddressBits();

	/// --- EEPROM Read/Write --- Data sheet 5.5 ---

	// Data sheet 5.6.1
	// Fast write, update input DAC registers from A to D sequentially
	// does not affect the EEPROM; using this method only the power-down
	// bits PD1 and PD0 and the 12 DAC output bits are writable
	void fastWrite(const uint16_t dataA, const uint16_t dataB, const uint16_t dataC, const uint16_t dataD);

	// Data sheet 5.6.2
	// Used to write DAC input register, one at a time; does not affect
	// the EEPROM. DAC select bits DAC1, DAC0 select which DAC to write.
	void multiWrite(const DAC::addressedData_v data);

	// Data sheet 5.6.3
	// Write DAC input registers and EEPROM sequentially from starting channel to last channel (D);
	// ie. send A, B, C, D (10 bytes) or B, C, D (8 bytes) or C, D (6 bytes) or D (4 bytes)
	void sequentialWrite(const uint8_t startAddr, const uint16_v data);

	// Data sheet 5.6.4
	// Single write; write to a single DAC register and EEPROM
	void singleWrite(const uint8_t dacAddr, const uint16_t dacValue);

private:
	uint8_v m_PDBits;	// 4x Power down bits registers, A (0) - D (3)
	uint8_v m_VrefBits;	// 4x Vref registers, A (0) - D (3)
	uint8_v m_GxBits;	// 4x Gx registers, A (0) - D (3)

	GPIOPin::shared_ptr m_pGPIO;
	I2C::shared_ptr m_pI2C;
};
