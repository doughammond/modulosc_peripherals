#pragma once

#include <boost/shared_ptr.hpp>

#include <QObject>
#include <QThreadPool>
#include <QRunnable>
#include <QSemaphore>
#include <QTimer>

// SPI device dependencies
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include "intarrays.h"

static const int BufferSize = 64;

typedef boost::shared_ptr<QSemaphore> QSemaphore_shared_ptr;

class KL25Z_SPI_Reader : public QRunnable {

public:
	typedef boost::shared_ptr<KL25Z_SPI_Reader> shared_ptr;

	KL25Z_SPI_Reader() :
		m_pFreeBytes(new QSemaphore(BufferSize)),
		m_pUsedBytes(new QSemaphore),
		m_pBuffer(new uint8_v(BufferSize)),
		status(0),
		spi_mode(SPI_MODE_3),
		spi_bitsPerWord(8),
		spi_speed(6000000),
		bufferIndex(0)
	{
		spi_fd = open("/dev/spidev0.0", O_RDWR);

		status |= ioctl(spi_fd, SPI_IOC_WR_MODE, &spi_mode);
		status |= ioctl(spi_fd, SPI_IOC_RD_MODE, &spi_mode);
		status |= ioctl(spi_fd, SPI_IOC_WR_BITS_PER_WORD, &spi_bitsPerWord);
		status |= ioctl(spi_fd, SPI_IOC_RD_BITS_PER_WORD, &spi_bitsPerWord);
		status |= ioctl(spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed);
		status |= ioctl(spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &spi_speed);
	}

	~KL25Z_SPI_Reader()
	{
		if (spi_fd) {
			close(spi_fd);
		}
	}

	QSemaphore_shared_ptr getFreeBytesSem() {
		return QSemaphore_shared_ptr(m_pFreeBytes);
	}

	QSemaphore_shared_ptr getUsedBytesSem() {
		return QSemaphore_shared_ptr(m_pUsedBytes);
	}

	uint8_v_shared_ptr getBuffer() {
		return uint8_v_shared_ptr(m_pBuffer);
	}

	virtual void run();

	char buffer[BufferSize];

private:
	QSemaphore_shared_ptr m_pFreeBytes;
	QSemaphore_shared_ptr m_pUsedBytes;

	uint8_v_shared_ptr m_pBuffer;

	int spi_fd;		// file descriptor for SPI device
	int status;
	unsigned char spi_mode;
	unsigned char spi_bitsPerWord;
	unsigned int spi_speed;

	unsigned int bufferIndex;
};


class KL25Z : public QObject
{
	Q_OBJECT

public:
	typedef boost::shared_ptr<KL25Z> shared_ptr;

	explicit KL25Z(QObject *parent = 0);

signals:
	void message(const QByteArray data);

public slots:
	void tmrGetData();

private:
	KL25Z_SPI_Reader::shared_ptr m_pReader;

	QTimer m_dataTimer;
	unsigned int bufferIndex;

};
