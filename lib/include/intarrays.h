#pragma once

#include <vector>
#include <stdint.h>

#include <boost/shared_ptr.hpp>

typedef std::vector<uint8_t> uint8_v;
typedef std::vector<uint16_t> uint16_v;


typedef boost::shared_ptr<uint8_v> uint8_v_shared_ptr;
