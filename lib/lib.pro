##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

QT -= gui

TARGET = ModulOSC_peripherals
TEMPLATE = lib

include(../../ModulOSC_Project.Cross.pri)

INCLUDEPATH += include

SOURCES += \
	src/GPIO.cpp \
	src/I2C.cpp \
	src/LCDDriver.cpp \
	src/MCP4728.cpp \
	src/KL25Z.cpp

HEADERS += \
	include/GPIO.h \
	include/I2C.h \
	include/LCDDriver.h \
	include/MCP4728.h \
	include/KL25Z.h \
	include/intarrays.h
