##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#-------------------------------------------------
#
# ModulOSC_perphierals / test : Perphieral I/O Tests
#
#-------------------------------------------------

QT       += core dbus network
QT       -= gui

TARGET = test_ModulOSC_peripherals
CONFIG   += console qtestlib
CONFIG   -= app_bundle x11

TEMPLATE = app

include(../../ModulOSC_Project.Cross.pri)

INCLUDEPATH += \
	../lib/include

SOURCES += \
	src/main.cpp \
	src/Mock_I2C.cpp \
	src/test_I2C.cpp \
	src/test_LCDDriver.cpp \
	src/test_MCP4728.cpp

HEADERS += \
	src/Mock_I2C.h \
	src/test_I2C.h \
	src/test_LCDDriver.h \
	src/test_MCP4728.h

LIBS += \
	$$DESTDIR/libModulOSC_peripherals.so
