//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QCoreApplication>

//#include "test_I2C.h"
//#include "test_MCP4728.h"
//#include "test_LCDDriver.h"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

/*
	test_I2C test_i2c;
	int result_i2c = QTest::qExec(&test_i2c, argc, argv);

	Test_MCP4728 test_dac;
	int result_dac = QTest::qExec(&test_dac, argc, argv);

	Test_LCDDriver test_lcd;
	int result_lcd = QTest::qExec(&test_lcd, argc, argv);

	return result_i2c && result_dac && result_lcd;
	*/

	return 0;
}
