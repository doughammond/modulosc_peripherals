//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "test_I2C.h"


void test_I2C::init() {
	m_pI2C.reset(new Mock_I2C(0x20));
	QCOMPARE(m_pI2C->getErrorMessage(), "");
}

void test_I2C::test_send_1_buffer() {
	uint8_t RxBuf[4] = { 0x01, 0x02, 0x03, 0x04 };
	int res = m_pI2C->send(RxBuf, 4);

	QVERIFY(res == 1);
	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   01 02 03 04 \n")
	);
}

void test_I2C::test_send_2_buffer_to_register() {
	uint8_t RxBuf[4] = { 0x01, 0x02, 0x03, 0x04 };
	int res = m_pI2C->send(0xAE, RxBuf, 4);

	QVERIFY(res == 1);
	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   ae 01 02 03 04 \n")
	);
}

void test_I2C::test_send_3_one_value() {
	int res = m_pI2C->send(0xDE);

	QVERIFY(res == 1);
	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBYD;   de \n")
	);
}

void test_I2C::test_send_4_one_value_to_register() {
	int res = m_pI2C->send(0xAE, 0xDE);

	QVERIFY(res == 1);
	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBYD;   ae de \n")
	);
}
