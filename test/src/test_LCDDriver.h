//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <QString>
#include <QtTest/QTest>

#define I2C_DEBUG 1

#include "LCDDriver.h"

#include "Mock_I2C.h"

static const QString _writePreamble = (
	"WBYD;   15 00 \n"
	"WBLD;   00 3f 10 3f 00 3f 00 00 00 00 00 02 02 3f 00 00 00 00 00 c0 ef c0 ef \n"
	"WBYD;   0a a2 \n"
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 39 19 39 19 \n"
	"WBLD;   19 39 19 29 09 \n"
	"WBLD;   19 29 09 23 03 \n"
	"WBLD;   19 21 01 31 11 \n"
	"WBYD;   10 10 \n"
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 31 11 25 05 \n"
	"WBLD;   19 21 01 2d 0d \n"
	"WBLD;   19 21 01 27 07 \n"
	"WBLD;   19 21 01 29 09 \n"
	"WBYD;   10 10 \n"
);

class Test_LCDDriver : public QObject
{
	Q_OBJECT

private slots:

	void init();

	void test_init();
	void test_stop();
	void test_write_1_string();
	void test_write_2_string_charmode();
	void test_write_3_block();
	void test_write_4_block_charmode();
	void test_write_5_value();
	void test_write_6_value_charmode();
	void test_begin();
	void test_clear();
	void test_home();
	void test_setCursor();
	void test_display();
	void test_noDisplay();

private:
	GPIOPin::shared_ptr m_pGPIO;
	Mock_I2C::shared_ptr m_pI2C;
	LCDDriver::shared_ptr m_pLCD;
};

