//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "test_MCP4728.h"


void Test_MCP4728::init() {
	m_pGPIO.reset(new GPIOPin(24, false));
	m_pI2C.reset(new Mock_I2C(0x20));
	m_pDAC.reset(new MCP4728(m_pGPIO, m_pI2C));

	QCOMPARE(m_pI2C->getErrorMessage(), "");
}

void Test_MCP4728::test_reset() {
	m_pDAC->reset();

	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	// XXX looks wrong !
	QString("WBYD;   00 \nWBYD;   06 \n")
	);
}

void Test_MCP4728::test_wakeUp() {
	m_pDAC->wakeUp();

	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	// XXX looks wrong !
	QString("WBYD;   00 \nWBYD;   09 \n")
	);
}

void Test_MCP4728::test_softwareUpdate() {
	m_pDAC->softwareUpdate();

	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	// XXX looks wrong !
	QString("WBYD;   00 \nWBYD;   08 \n")
	);
}

void Test_MCP4728::test_readAddressBits() {
	m_pDAC->readAddressBits();

	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	// XXX looks wrong !
	QString("WBYD;   00 \nWBYD;   0c \nWBYD;   c1 \nRB; 1\n")
	);
}

void Test_MCP4728::test_fastWrite() {
	m_pDAC->fastWrite(0x01, 0x02, 0x03, 0x04);

	QCOMPARE(m_pI2C->getErrorMessage(), "");
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 00 01 00 02 00 03 00 04 \n")
	);
}

void Test_MCP4728::test_multiWrite() {
	DAC::addressedData_v data;
	data += DAC::addressedData_t(0x00, 0x0123);
	data += DAC::addressedData_t(0x00, 0x0234);
	data += DAC::addressedData_t(0x01, 0x0345);
	data += DAC::addressedData_t(0x01, 0x0456);
	m_pDAC->multiWrite(data);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 40 01 23 40 02 34 42 03 45 42 04 56 \n")
	);
}

void Test_MCP4728::test_sequentialWrite_4() {
	uint16_v data;
	data += 0x0123; data += 0x0234; data += 0x0345; data += 0x0456;
	m_pDAC->sequentialWrite(0x00, data);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 50 01 23 02 34 03 45 04 56 \n")
	);
}

void Test_MCP4728::test_sequentialWrite_3() {
	uint16_v data;
	data += 0x0123; data += 0x0234; data += 0x0345;
	m_pDAC->sequentialWrite(0x01, data);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 52 01 23 02 34 03 45 \n")
	);
}

void Test_MCP4728::test_sequentialWrite_2() {
	uint16_v data;
	data += 0x0123; data += 0x0234;
	m_pDAC->sequentialWrite(0x02, data);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 54 01 23 02 34 \n")
	);
}

void Test_MCP4728::test_sequentialWrite_1() {
	uint16_v data;
	data += 0x0123;
	m_pDAC->sequentialWrite(0x03, data);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 56 01 23 \n")
	);
}

void Test_MCP4728::test_singleWrite() {
	m_pDAC->singleWrite(0x00, 0x0123);

	QVERIFY(!m_pI2C->fail());
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString("WBLD;   c0 58 01 23 \n")
	);
}
