//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/shared_ptr.hpp>

#include "I2C.h"

class Mock_I2C : public I2C
{
public:
	typedef boost::shared_ptr<Mock_I2C> shared_ptr;

	explicit Mock_I2C(int Address);

	virtual std::string getDebugOutput();

	virtual int receive(unsigned char *RxBuf, int length);
	virtual int receive(unsigned char RegisterAddress, unsigned char *RxBuf, int length);
	virtual int send(unsigned char *TxBuf, int length);
	virtual int send(unsigned char RegisterAddress, unsigned char *TxBuf, int length);
	virtual int send(unsigned char value);
	virtual int send(unsigned char RegisterAddress, unsigned char value);

protected:

	virtual int open_fd();
	virtual void close_fd() {}

private:

	std::stringstream m_dbstream;

	virtual void _outChar(const unsigned char c);

};
