//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "test_LCDDriver.h"


void Test_LCDDriver::init() {
	m_pGPIO.reset(new GPIOPin(23, true));
	m_pI2C.reset(new Mock_I2C(0x20));
	m_pLCD.reset(new LCDDriver(m_pGPIO, m_pI2C));
}

void Test_LCDDriver::test_init() {
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble
	)
	);
}

void Test_LCDDriver::test_stop() {
	m_pLCD->stop();
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   15 00 \n"
	"WBLD;   00 3f 10 00 00 00 00 00 00 00 00 00 00 3f 00 00 00 00 00 c0 01 c0 01 \n"
	)
	);
}

void Test_LCDDriver::test_write_1_string() {
	m_pLCD->write(std::string("TEST"), false);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 35 15 25 05 25 05 35 15 35 15 39 19 35 15 25 05 \n"
	)
	);
}

void Test_LCDDriver::test_write_2_string_charmode() {
	m_pLCD->write(std::string("TEST"), true);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 b5 95 a5 85 a5 85 b5 95 b5 95 b9 99 b5 95 a5 85 \n"
	)
	);
}

void Test_LCDDriver::test_write_3_block() {
	uint8_v data;
	data += 0x00; data += 0x01; data += 0x03; data += 0x04;
	m_pLCD->write(data, false);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 21 01 21 01 31 11 21 01 39 19 21 01 25 05 \n"
	)
	);
}

void Test_LCDDriver::test_write_4_block_charmode() {
	uint8_v data;
	data += 0x00; data += 0x01; data += 0x03; data += 0x04;
	m_pLCD->write(data, true);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 a1 81 a1 81 a1 81 b1 91 a1 81 b9 99 a1 81 a5 85 \n"
	)
	);
}

void Test_LCDDriver::test_write_5_value() {
	m_pLCD->write(0xAE, false);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 2b 0b 2f 0f \n"
	)
	);
}

void Test_LCDDriver::test_write_6_value_charmode() {
	m_pLCD->write(0xAE, true);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 ab 8b af 8f \n"
	)
	);
}

void Test_LCDDriver::test_begin() {
	// XXX actually only sends the clear() command
	m_pLCD->begin(16, 2);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 31 11 \n"
	"WBYD;   10 10 \n"
	)
	);
}

void Test_LCDDriver::test_clear() {
	m_pLCD->clear();
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 31 11 \n"
	"WBYD;   10 10 \n"
	)
	);
}

void Test_LCDDriver::test_home() {
	m_pLCD->home();
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 29 09 \n"
	"WBYD;   10 10 \n"
	)
	);
}

void Test_LCDDriver::test_setCursor() {
	m_pLCD->setCursor(8, 2);
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 3f 1f 27 07 \n"
	)
	);
}

void Test_LCDDriver::test_display() {
	m_pLCD->display();
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 27 07 \n"
	)
	);
}

void Test_LCDDriver::test_noDisplay() {
	m_pLCD->noDisplay();
	QCOMPARE(
	QString(m_pI2C->getDebugOutput().c_str()),
	QString(
	_writePreamble +
	"WBYD;   19 41 \n"
	"WBYD;   19 61 \n"
	"RB; 1\n"
	"WBLD;   19 41 61 41 \n"
	"WBYD;   10 00 \n"
	"WBLD;   19 21 01 23 03 \n"
	)
	);
}
