//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Mock_I2C.h"

Mock_I2C::Mock_I2C(int Address) :
	I2C(Address)
{
}

std::string Mock_I2C::getDebugOutput() {
	// gets the string buffer and resets it
	std::string data = m_dbstream.str();
	m_dbstream.clear();
	return data;
}

int Mock_I2C::receive(unsigned char RegisterAddress, unsigned char *RxBuf, int length) {
	for (int i=0; i<length; ++i) {
		RxBuf[i] = 0x00;
	}
	m_dbstream << "RBA; ";
	_outChar(RegisterAddress);
	m_dbstream << " " << length << std::endl;
	return 1;
}

int Mock_I2C::send(unsigned char RegisterAddress, unsigned char value) {
	m_dbstream << "WBYD;   ";
	_outChar(RegisterAddress);
	_outChar(value);
	m_dbstream << std::endl;
	return 1;
}

int Mock_I2C::open_fd() {
	m_dbstream << "Open " << devicefile << " address " << std::hex << static_cast<int>(slave_address) << std::dec << std::endl;
	return 1;
}

void Mock_I2C::_outChar(const unsigned char c) {
	char bf[8];
	sprintf(bf, "%02x ", c);
	m_dbstream << std::string(bf);
	m_dbstream.flush();
}

int Mock_I2C::send(unsigned char value) {
	m_dbstream << "WBYD;   ";
	_outChar(value);
	m_dbstream << std::endl;
	return 1;
}

int Mock_I2C::send(unsigned char RegisterAddress, unsigned char *TxBuf, int length) {
	m_dbstream << "WBLD;   ";
	_outChar(RegisterAddress);
	for (int i=0; i<length; ++i) {
		_outChar(TxBuf[i]);
	}
	m_dbstream << std::endl;
	return 1;
}

int Mock_I2C::send(unsigned char *TxBuf, int length) {
	m_dbstream << "WBLD;   ";
	for (int i=0; i<length; ++i) {
		_outChar(TxBuf[i]);
	}
	m_dbstream << std::endl;
	return 1;
}

int Mock_I2C::receive(unsigned char *RxBuf, int length) {
	for (int i=0; i<length; ++i) {
		RxBuf[i] = 0x00;
	}
	m_dbstream << "RB; " << length << std::endl;
	return 1;
}
