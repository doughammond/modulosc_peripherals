//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <QString>
#include <QtTest/QTest>

#define I2C_DEBUG 1

#include "Mock_I2C.h"

class test_I2C : public QObject
{
	Q_OBJECT


private slots:

	void init();

	void test_send_1_buffer();
	void test_send_2_buffer_to_register();
	void test_send_3_one_value();
	void test_send_4_one_value_to_register();

private:
	Mock_I2C::shared_ptr m_pI2C;
};

