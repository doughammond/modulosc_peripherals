//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QCoreApplication>

#include <math.h>

#include "MCP4728.h"

#include <time.h>

static int64_t
get_elapsed_time(const struct timespec * /*restrict*/ start_time,
				 const struct timespec * /*restrict*/ end_time)
{
	int64_t sec = end_time->tv_sec - start_time->tv_sec;
	int64_t nsec;
	if (end_time->tv_nsec >= start_time->tv_nsec) {
		nsec = end_time->tv_nsec - start_time->tv_nsec;
	} else {
		nsec = 1000000000 - (start_time->tv_nsec - end_time->tv_nsec);
		sec -= 1;
	}
	return nsec; // > 0 ? sec * 1000000000 : 0 + nsec > 0 ? nsec : 0;
}

void chkI2C(I2C::shared_ptr pI2C){
	if (pI2C->fail()) {
		std::cout << "I2C " << pI2C->getErrorMessage() << std::endl;
	}
}

static const double M_2PI = M_PI * 2.0;

float _sn(float t) {
	return (sin(M_2PI*t)+1)/2;
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	GPIOPin::shared_ptr pGPIO(new GPIOPin(24, false));

	I2C::shared_ptr pI2C(new I2C(0x60));
	chkI2C(pI2C);

	MCP4728::shared_ptr pDAC(new MCP4728(pGPIO, pI2C));
	chkI2C(pI2C);


	struct timespec ref_time;
	struct timespec cur_time;
	clock_gettime(CLOCK_MONOTONIC_RAW, &ref_time);

	int64_t nsec = 0;
	double ph = 0.0;
	uint16_t valA = 0;
	uint16_t valB = 0;
	uint16_t valC = 0;
	uint16_t valD = 0;

	while (true) {
		clock_gettime(CLOCK_MONOTONIC_RAW, &cur_time);
		nsec = get_elapsed_time(&ref_time, &cur_time);

		ph = 3.0 * (nsec/1000000000.0);

		valA = _sn(ph + 0.00) * 0xFFF;
		valB = _sn(ph + 0.25) * 0xFFF;
		valC = _sn(ph + 0.50) * 0xFFF;
		valD = _sn(ph + 0.75) * 0xFFF;

		pDAC->fastWrite(valA, valB, valC, valD);
	}

	// return a.exec();
}
